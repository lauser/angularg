import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {

  m: any= {
    mensaje : ''
  };
  constructor(private generalService : GeneralService) { }

  ngOnInit() {
    this.getMessage()
  }

  getMessage(){
    this.generalService.getMessageFromServer().subscribe(
      res => {
        this.m = res ;
        console.log(this.m);
      },
      err => console.log(err)
    );
  }
}
